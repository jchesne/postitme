<?php
/**
 * @file
 * Contains theme override functions and preprocess functions for the theme.
 *
 * ABOUT THE TEMPLATE.PHP FILE
 *
 *   The template.php file is one of the most useful files when creating or
 *   modifying Drupal themes. You can modify or override Drupal's theme
 *   functions, intercept or make additional variables available to your theme,
 *   and create custom PHP logic. For more information, please visit the Theme
 *   Developer's Guide on Drupal.org: http://drupal.org/theme-guide
 *
 * OVERRIDING THEME FUNCTIONS
 *
 *   The Drupal theme system uses special theme functions to generate HTML
 *   output automatically. Often we wish to customize this HTML output. To do
 *   this, we have to override the theme function. You have to first find the
 *   theme function that generates the output, and then "catch" it and modify it
 *   here. The easiest way to do it is to copy the original function in its
 *   entirety and paste it here, changing the prefix from theme_ to STARTERKIT_.
 *   For example:
 *
 *     original: theme_breadcrumb()
 *     theme override: STARTERKIT_breadcrumb()
 *
 *   where STARTERKIT is the name of your sub-theme. For example, the
 *   zen_classic theme would define a zen_classic_breadcrumb() function.
 *
 *   If you would like to override either of the two theme functions used in Zen
 *   core, you should first look at how Zen core implements those functions:
 *     theme_breadcrumbs()      in zen/template.php
 *     theme_menu_local_tasks() in zen/template.php
 *
 *   For more information, please visit the Theme Developer's Guide on
 *   Drupal.org: http://drupal.org/node/173880
 *
 * CREATE OR MODIFY VARIABLES FOR YOUR THEME
 *
 *   Each tpl.php template file has several variables which hold various pieces
 *   of content. You can modify those variables (or add new ones) before they
 *   are used in the template files by using preprocess functions.
 *
 *   This makes THEME_preprocess_HOOK() functions the most powerful functions
 *   available to themers.
 *
 *   It works by having one preprocess function for each template file or its
 *   derivatives (called template suggestions). For example:
 *     THEME_preprocess_page    alters the variables for page.tpl.php
 *     THEME_preprocess_node    alters the variables for node.tpl.php or
 *                              for node-forum.tpl.php
 *     THEME_preprocess_comment alters the variables for comment.tpl.php
 *     THEME_preprocess_block   alters the variables for block.tpl.php
 *
 *   For more information on preprocess functions and template suggestions,
 *   please visit the Theme Developer's Guide on Drupal.org:
 *   http://drupal.org/node/223440
 *   and http://drupal.org/node/190815#template-suggestions
 */

function postitme_css_alter(&$css) {

  unset($css[drupal_get_path('module', 'system') . '/system.base.css']);
  unset($css[drupal_get_path('module', 'system') . '/system.menus.css']);
  unset($css[drupal_get_path('module', 'system') . '/system.messages.css']);
  unset($css[drupal_get_path('module', 'system') . '/system.theme.css']);

  unset($css[drupal_get_path('module', 'ckeditor') . '/css/ckeditor.css']);
  unset($css[drupal_get_path('module', 'ctools') . '/css/ctools.css']);
  unset($css[drupal_get_path('module', 'webform') . '/css/webform.css']);

  unset($css[drupal_get_path('module', 'field') . '/theme/field.css']);
  unset($css[drupal_get_path('module', 'node') . '/node.css']);
  unset($css[drupal_get_path('module', 'search') . '/search.css']);
  unset($css[drupal_get_path('module', 'user') . '/user.css']);
  unset($css[drupal_get_path('module', 'views') . '/css/views.css']);
  unset($css[drupal_get_path('module', 'date') . '/date_api/date.css']);

}

/**
 * Override or insert variables into the page templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("page" in this case.)
 */
function postitme_preprocess_page(&$vars) {

  $path_url = drupal_get_path('theme', 'postitme');


  drupal_add_js($path_url . '/js/jquery.lettering.js', 'file');
  drupal_add_js($path_url . '/js/jquery-ui.min.js', 'file');
  drupal_add_js($path_url . '/js/bottomScript.js', array(
    'type' => 'file',
    'scope' => 'footer',
    'weight' => 5,
  ));

  drupal_add_js($path_url . '/js/jquery.validate.min.js', 'file');

  //add or not the javascript.
  if (drupal_is_front_page()) {
    drupal_add_js($path_url . '/js/jquery-css-transform.js', 'file');
    drupal_add_js($path_url . '/js/home-script.js', array(
      'type' => 'file',
      'scope' => 'footer',
      'weight' => 5,
    ));
    drupal_add_js($path_url . '/js/jquery.quicksand.js', 'file');
  }
  else {
    //drupal_add_js($path_url . '/js/history/jquery.history.js', 'file');
    //drupal_add_js($path_url . '/js/ajaxify-html5.js', 'file');

    drupal_add_js($path_url . '/js/projet.js', array(
      'type' => 'file',
      'scope' => 'footer',
      'weight' => 5,
    ));
  }

  if (isset($vars['node']->type)) { // We don't want to apply this on taxonomy or view pages
    // Splice (2) is based on existing default suggestions. Change it if you need to.
    array_splice($vars['theme_hook_suggestions'], -1, 0, 'page__' . $vars['node']->type);
    // Get the url_alias and make each item part of an array
    $url_alias = drupal_get_path_alias($_GET['q']);
    $split_url = explode('/', $url_alias);
    // Add the full path template pages
    // Insert 2nd to last to allow page--node--[nid] to be last
    $cumulative_path = '';
    foreach ($split_url as $path) {
      $cumulative_path .= '__' . $path;
      $path_name = 'page' . $cumulative_path;
      array_splice($vars['theme_hook_suggestions'], -1, 0, str_replace('-', '_', $path_name));
    }
    // This does just the page name on its own & is considered more specific than the longest path
    // (because sometimes those get too long)
    // Also we don't want to do this if there were no paths on the URL
    // Again, add 2nd to last to preserve page--node--[nid] if we do add it in
    if (count($split_url) > 1) {
      $page_name = end($split_url);
      array_splice($vars['theme_hook_suggestions'], -1, 0, 'page__' . str_replace('-', '_', $page_name));
    }
  }
}


function node_sibling($dir = 'next', $node, $next_node_text = NULL, $prepend_text = NULL, $append_text = NULL, $tid = FALSE) {
  if ($tid) {
    $query = 'SELECT n.nid, n.title FROM {node} n INNER JOIN {term_node} tn ON n.nid=tn.nid WHERE '
      . 'n.nid ' . ($dir == 'previous' ? '<' : '>') . ' :nid AND n.type = :type AND n.status=1 '
      . 'AND tn.tid = :tid ORDER BY n.nid ' . ($dir == 'previous' ? 'DESC' : 'ASC');
    //use fetchObject to fetch a single row 
    $row = db_query($query, array(
      ':nid' => $node->nid,
      ':type' => $node->type,
      ':tid' => $tid
    ))->fetchObject();
  }
  else {
    $query = 'SELECT n.nid, n.title FROM {node} n WHERE '
      . 'n.nid ' . ($dir == 'previous' ? '<' : '>') . ' :nid AND n.type = :type AND n.status=1 '
      . 'ORDER BY n.nid ' . ($dir == 'previous' ? 'DESC' : 'ASC');
    //use fetchObject to fetch a single row     
    $row = db_query($query, array(
      ':nid' => $node->nid,
      ':type' => $node->type
    ))->fetchObject();
  }

  if ($row) {
    $text = $next_node_text ? $next_node_text : $row->title;
    return $prepend_text . l($text, 'node/' . $row->nid, array(
      'attributes' =>
        array(
          'id' => $dir . '_button'
        )
    )) . $append_text;
  }
  else {
    return FALSE;
  }
}
