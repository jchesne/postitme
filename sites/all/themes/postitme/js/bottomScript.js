(function ($) {

  var projet = (($('body.node-type-projet').length) ? true : false);

  //Au click sur Contact on affiche le menu de contact
  $('.menu li.nolink span').click(function () {
    //On séléctionne l'item de menu actif courant
    var $currentActive = $('.menu .active');

    //Si l'item actif est "Contact" alors on veut le fermer
    if ($currentActive.text() == $(this).text()) {
      var $pathname = window.location.pathname;

      //On enlève la classe active sur "Contact"
      //desactiveButton($(this));

      //On regarde sur quelle URL nous sommes ex:<front> et dans se cas la on active "Home"
      /* $('.menu a').each(function (index) {
       if ($(this).attr('href') == pathname) {
       //On ajoute la classe active sur le bouton que l'on viens de cliquer
       // activeButton($(this));
       }
       });*/


      $(".menu a[href='" + $pathname + "']").addClass('active');
      $(this).removeClass('active');

      //On desactive le block de contact
      desactiveContact();

      //Si le lien "Contact" n'est pas encore actif alors on l'active
    }
    else {
      //On désactive le bouton ancienement cliqué pour activé Contact
      //desactiveButton($currentActive);
      $currentActive.removeClass("active");
      $(this).addClass('active');
      //On active Contact
      //activeButton($(this));
      //On active le block de contact
      activeContact();
    }
  });

  //Au click sur les items du menu
  $('.menu li a').click(function (event) {
    var pathname = window.location.pathname;
    var $currentActive = $('.menu .active');

    if (pathname == $(this).attr("href") && $currentActive.text() != $(this).text()) {
      $(this).addClass('active');
      //desactiveButton($currentActive);

      //On désactive le menu "Contact" si il est actif
      if ($currentActive.text() == $('.menu li.nolink span').text()) {
        //On desactive le block de contact
        desactiveContact();
      }
      return false;
    }
  });

  /* $('ul.menu a, ul.menu span').hover(function () {
   if (!$(this).hasClass('active')) {
   $(this).stop().animate({
   backgroundColor: "black",
   color: "white"
   }, 400);
   }
   }, function () {
   if (!$(this).hasClass('active')) {
   $(this).stop().animate({
   backgroundColor: "white",
   color: "#606060"
   }, 400);
   }

   });*/

  /*  function activeButton(button) {
   button.stop().animate({
   backgroundColor: "black",
   color: "white"
   }, 400, function () {
   button.addClass('active');
   });
   }

   function desactiveButton(button) {
   button.stop().animate({
   backgroundColor: "white",
   color: "#606060"
   }, 400, function () {
   button.removeClass('active');
   });
   }*/

  function activeContact() {
    $('#header').stop().animate({
      'height': (projet) ? 488 : 541,
    }, 1000, 'easeInOutExpo');
  }

  function desactiveContact() {
    $('#header').stop().animate({
      'height': 80
    }, 1000, 'easeInOutExpo');
  }

  /********************************
   GESTION EMAIL
   ********************************/
  $("#edit-actions").append('<div id="contact_confirmation"></div>');

  //validate the form

  $("#contact form").validate({
    //set the rules for the fild names
    rules: {
      "submitted[email_]": {
        email: true,
        required: true
      },
      "submitted[message_]": {
        required: true
      },
      "submitted[nom]": {
        required: true
      }
    },
    //set messages to appear inline
    messages: {
      "submitted[email_]": "",
      "submitted[message_]": "",
      "submitted[nom]": ""
    },

    submitHandler: function () {

      var $webform = $('#webform-client-form-101');
      var datas = $webform.serialize();
      var action = $webform.attr("action");

      var zone_message_retour = $("#contact_confirmation");
      zone_message_retour.html('<div id="loader"></div>');
      zone_message_retour.fadeIn(5000);
      $("#edit-submit").attr('disabled', 'disabled');

      $.ajax({
        type: 'POST',
        url: action,
        data: datas,
        context: document.body,
        success: function (retour) {
          $("#edit-submit").attr('disabled', '');
          zone_message_retour.html('Message bien envoyé !').delay(3500).queue(function () {
            desactiveContact();
            desactiveButton($('.menu li.nolink span'));
            var pathname = window.location.pathname
            //On regarde sur quelle URL nous sommes ex:<front> et dans se cas la on active "Home"
            $('.menu a').each(function (index) {
              if ($(this).attr('href') == pathname) {
                //On ajoute la classe active sur le bouton que l'o viens de cliquer
                activeButton($(this));
              }
            });

          });


        }
      });
    }
  });


  /********************************
   FIN GESTION EMAIL
   ********************************/

  /********************************
   GESTION LOGO
   ********************************/
  $(".logo").lettering('letters');
  /********************************
   FIN GESTION LOGO
   ********************************/

  $('.field-name-field-projet-categorie a[href*="/%23"]').each(function (index) {
    $(this).attr('href', '/#' + $(this).attr('href').substr(4));
  });

  /********************************
   GESTION Over Projet
   ********************************/


  /* ROLL OVER */

  //Gestion du roll Over sur les vignettes de la home et des pages de projet
  $('.folio_img').live('mouseover mouseout', function (event) {
    if (event.type == 'mouseover') {
      $(this).find("img,span").stop().animate({
        "margin-top": -20
      }, 500, 'easeOutCirc');
    }
    else {
      $(this).find("img,span").stop().animate({
        "margin-top": 0
      }, 500, 'easeInCirc');
    }
  });
  /********************************
   GESTION Over Projet
   ********************************/
})(jQuery);