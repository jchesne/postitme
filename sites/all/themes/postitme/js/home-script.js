(function ($) {


  /********************************
   GESTION DU BANDEAU LAB
   ********************************/

  /*
   $("#lab_button").click(function (event) {
   if ($(this).hasClass('active')) {
   window.location.hash = "";

   $(this).removeClass('active');
   $(this).css('position', 'fixed');
   $(this).stop().animate({top: 11}, 1000, 'easeInOutExpo');
   $(this).parent().stop().animate({height: 20}, 1000, 'easeInOutExpo');
   $("#header").stop().animate({top: 20}, 1000, 'easeInOutExpo', function () {
   $(this).css('position', 'fixed')
   });

   }
   else {
   window.location.hash = "labs";

   $(this).addClass('active');
   $(this).stop().animate({top: 240}, 1000, 'easeInOutExpo', function () {
   $(this).css('position', 'absolute')
   });
   $(this).parent().stop().animate({height: 240}, 1000, 'easeInOutExpo');
   $("#header").stop().animate({top: 240}, 1000, 'easeInOutExpo', function () {
   $(this).css('position', 'absolute')
   });
   }

   });
   */

  /********************************
   FIN GESTION DU BANDEAU LAB
   ********************************/


  /********************************
   GESTION DE LA GALERIE
   ********************************/
  /* FILTRE */
  var $applications = $('.view-home-projet .view-content'),
    $data = $applications.clone(),
    $activeFilter;

  $('.view-cat-gories a').on('click', function (event) {
    // Act on the event
    if ($activeFilter) {
      $activeFilter.removeClass('active');
    }

    $activeFilter = $(this);
    $activeFilter.addClass('active');

    window.location.hash = $activeFilter.attr('href');

    var $type = $activeFilter.attr('href').substr(1);
    if ($type == "Toutes") {
      var $selectedFolio = $data.find(".views-row");
    }
    else {
      var $selectedFolio = $data.find(".views-row span:contains('" + $type + "')").parent().parent();
    }

    $applications.quicksand($selectedFolio, {
      adjustHeight: 'dynamic',
      duration: 800,
      useScaling: true,
      easing: 'easeInOutQuad',
      attribute: function (v) {
        return $(v).find('img').attr('src');
      }
    });
    event.preventDefault();
  });

  //On vérifie si l'URL contient un hash

  //Ici gestion ajax suivant, precédent
  // Check for hash value in URL

  var hash = window.location.hash,
    $selectedFilter = $(".view-cat-gories a[href='" + hash + "']");
  if ($selectedFilter.length && hash != "#Toutes") {
    $selectedFilter.click();
  }
  else {
    if (hash == "#labs") {
      $("#lab_button").click();
    }
    else {
      //On active le premier filtre "Tous"
      $('div.view-cat-gories div.views-row:first-child a').click();
    }
  }


  /********************************
   FIN GESTION DE LA GALERIE
   ********************************/


  /********************************
   GESTION LETTERING
   ********************************/

  //$("#drupal_infos span.desc").lettering('lines');

  /********************************
   FIN GESTION LETTERING
   ********************************/

  /********************************
   GESTION ASSOCIATE
   ********************************/
  /*
   var $sayyes = $('#sayyes');
   var $famous = $('#famous');

   $('#sayyes,#famous').bind('mouseover', gestionHover);
   $('#sayyes,#famous').bind('mouseout', gestionOut);

   var $currentActive;

   function gestionHover(event) {
   $currentActive = $(this);

   if ($(this).get(0) == $sayyes.get(0)) {
   cache($famous);
   }
   else {
   cache($sayyes);
   }
   }

   function gestionOut(event) {
   if ($(this).get(0) == $sayyes.get(0)) {
   affiche($famous);
   }
   else {
   affiche($sayyes);
   }

   }


   function cache($associate) {
   $associate.stop().animate({'opacity': .5}, 1000);
   }

   function affiche($associate) {
   $associate.stop().animate({'opacity': 1}, 1000);
   }
   */
  /********************************
   FIN GESTION ASSOCIATE
   ********************************/
})(jQuery);
