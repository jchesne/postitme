<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div id='header_projet'>
    <h1><?php echo $title; ?></h1><?php $next = node_sibling('next', $node, NULL, NULL, NULL, FALSE);
    $previous = node_sibling('previous', $node, NULL, NULL, NULL, FALSE);
    print '<div id="projets_navigator">';
    if ($previous) {
      print ' <div id="previous_container"><div id="previous"><span class=base_text>Projet précédent</span><span class=previous>' . $previous . '</span></div></div>';
    }
    if ($next) {
      print ' <div id="next_container"><div id="next"><span class=base_text>Projet suivant</span><span class=next>' . $next . '</span></div></div>';
    }
    print '</div>'; ?></div>
  <div class="content">
    <div class="left_details"><?php print render($content['field_projet_url']);
      print render($content['field_projet_categorie']);
      print render($content['field_projet_techno']);
      echo '</br>';
      print render($content['body']); ?></div>
    <div id="preview"><?php print render($content['field_projet_captures']);
      print render($content['field_projet_url']); ?></div><?php if ($node->field_projet_associes) { ?>
      <div class="left_details associate">
      <p>Projets similaires</p><?php foreach ($node->field_projet_associes["fr"] as $sameProject) {
        $nodeId = $sameProject["nid"];
        $loadedNode = node_load($sameProject["nid"]);
        $alias = drupal_lookup_path('alias', "node/" . $nodeId);
        echo "<div class=sameProject><div class=views-field-field-projet-miniature><div class='folio_img'><a href=" . $alias . "><div>" . theme('image', array('path' => $loadedNode->field_projet_miniature["fr"][0]['uri'])) . "</div><span></span></a></div><div class='shadow'></div></div></div>";
      } ?></div><?php } ?>
    <div class="clearfix"></div>
  </div>
</div>