<div id="page-wrapper">
  <div id="page">
    <div id="header">
      <div class="section clearfix">
        <a href='http://postitme.com' class="home-link"><span class='logo'>postitme</span></a><?php print render($page['header']); ?>
        <div id="contact"><?php $node = node_load(101);
          echo "<div id=contact_intro>" . $node->body['fr'][0]['value'] . "</div>";
          $block = module_invoke('webform', 'block_view', 'client-block-101');
          print render($block['content']); ?>
          <div class='clearfix'></div>
        </div>
      </div>
    </div>
    <div id="main-wrapper">
      <div id="content" class="column">
        <?php print render($page['folio_home']); ?><?php print render($page['content']); ?>
      </div>
      <div id="clearfix"></div>
    </div><?php print render($page['footer']); ?></div>
</div><?php print render($page['bottom']); ?>
