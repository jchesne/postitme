<!doctype html>

<!--[if IE 7]>
<html class="no-js ie7 oldie" lang="<?php print $language->language; ?>" <?php print $rdf_namespaces; ?> <?php print $html_attributes; ?>> <![endif]-->
<!--[if IE 8]>
<html class="no-js ie8 oldie" lang="<?php print $language->language; ?>" <?php print $rdf_namespaces; ?> <?php print $html_attributes; ?>> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="<?php print $language->language; ?>" <?php print $rdf_namespaces; ?> <?php print $html_attributes; ?>> <!--<![endif]-->
<?php print $mothership_poorthemers_helper; ?>
<head>
  <title><?php print $head_title; ?></title>
  <meta name="google-site-verification" content="sIZ4WI0s3TyxRQJ4b1n_4bbe4H11l4_E8R5C6ZVmPYA"/>
  <meta property="og:image" content="http://www.postitme.com/sites/all/themes/postitme/img/icone.jpg"/>
  <?php print $head; ?>
  <?php print $appletouchicon; ?>
  <?php if (theme_get_setting('mothership_mobile')) { ?>
    <meta name="MobileOptimized" content="width">
    <meta name="HandheldFriendly" content="true"><?php } ?>
  <?php if (theme_get_setting('mothership_viewport')) { ?>
    <meta name="viewport" content="width=device-width, initial-scale=1"><?php } ?>
  <?php if (theme_get_setting('mothership_viewport_maximumscale')) { ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"><?php } ?>
  <meta http-equiv="cleartype" content="on">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <?php print $styles; ?>
  <?php if (theme_get_setting('mothership_respondjs')) { ?>
    <!--[if lt IE 9]>
    <script src="<?php print $mothership_path; ?>/js/respond.min.js"></script>
    <![endif]-->
  <?php } ?>
  <!--[if lt IE 9]>
  <script src="<?php print $mothership_path; ?>/js/html5.js"></script>
  <![endif]-->
  <?php print $selectivizr; ?>
  <?php
  if (!theme_get_setting('mothership_script_place_footer')) {
    print $scripts;
  }
  ?>
</head>
<body class="<?php print $classes; ?>" <?php print $attributes; ?>>
<div id="black_banner">
  <div class="inner">Dernière mise à jour le 15 Janvier 2016 | Suivez moi sur
    <a href="https://twitter.com/jerome_chesne" target="_blank">Twitter </a>|
    <a href="http://postitme.com/sites/default/files/CV%20-%20Jerome%20Chesne%20-%20DRUPAL.pdf"> CV </a>
  </div>
</div>
<div id="contact_mobile">
  <a href="mailto:contact@postitme.com">Me contacter par email</a>
  - Tel : 07 60 70 16 54
</div>

<?php /*if ($is_front) { */ ?><!--
  <div id="lab_wrapper">
    <div id="lab_button"></div>
    <div id="lab_container">
      <div id="lab_content"><h3>Templates :</h3>
        <ul>
          <li><a href="http://themes.postitme.com/perfectfolio" target="_blank"><img src="http://postitme.com/sites/all/themes/postitme/img/previewPerfectfolio.png" target="_blank" alt="Perfect Folio" width="243" height="116"></img></a></li>
        </ul>
      </div>
    </div>
  </div>--><?php /*} */ ?>
<?php print $page_top; //stuff from modules always render first ?>
<?php print $page; // uses the page.tpl ?>
<script type="text/javascript">var _gaq = _gaq || [];
  _gaq.push(["_setAccount", "UA-7492101-3"]);
  _gaq.push(["_trackPageview"]);
  (function () {
    var b = document.createElement("script");
    b.type = "text/javascript";
    b.async = true;
    b.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
    var a = document.getElementsByTagName("script")[0];
    a.parentNode.insertBefore(b, a)
  })();</script>
<?php
if (theme_get_setting('mothership_script_place_footer')) {
  print $scripts;
}
?>
<?php print $page_bottom; //stuff from modules always render last ?>
</body>
</html>

